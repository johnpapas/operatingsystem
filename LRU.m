function status = pageFaults(pageReq, nFrame)
% PAGEFAULTS - Compute number of page faults for LRU
%   
% STATUS = PAGEFAULTS( PAGERREQ, NFRAME ) simulates a Least
% Recenlty Used page reference policy with NFRAME number of
% frames. At each time step, the funciton is called with the next
% page request in PAGEREQ. The function returns either 'M' or 'H'
% if it was a miss or a hit, respectively. The function stores an
% internal state, using persistent variables, to store previously
% stored pages. At the beginning, consider every frame empty.

  persistent frames;
  persistent front=1;
  persistent rear;
  
  if isempty(frames)
     frames=NaN(nFrame,1);
  end
  
  if isempty(rear)
     rear=1;
  end
  
  if rear!=nFrame
     for(i=1:rear)
         if frames(i)==pageReq
             temp1=frames(i);
             temp2=frames(:);
             for(j=2:i)
                 frames(j)=temp2(j-1);
             end
             frames(front)=temp1;
             status='H';
             return
         end
     end
     temp3=frames(:);
     for(i=2:(rear+1))
         frames(i)=temp3(i-1);
     end
     frames(front)=pageReq;
     rear=rear+1;
     status='M';
     return 
  else
    for(i=1:nFrame)
         if frames(i)==pageReq
             temp1=frames(i);
             temp2=frames(:);
             for(j=2:i)
                 frames(j)=temp2(j-1);
             end
             frames(front)=temp1;
             status='H';
             return
         end
     end
     temp=frames(:);
     for(i=2:nFrame)
        frames(i)=temp(i-1);
     end
     frames(front)=pageReq;
     status='M';
     return
  end

end
