function curProc = sjf(arrival, flagPreempt)
% SJF - Simulate Shortest Job First scheduling policy
%   
% ARRIVAL is a cell array with two elements: The process name and
% its duration. If no process arrives at a certain timestep, the
% arrival is empty.
% FLAGPREEMPT states whether the policy is preemptive or not.
% 
% The function returns a string in CURPROC with the name of the
% process currently scheduled. If no process is scheduled, return '_'.

  %% DECLARE PERSISTENT VALUES
persistent procDurV;
persistent namesV;
persistent i;
persistent I;
  
  %% INITIALIZATION
if isempty(procDurV)
        procDurV = [];
        i=1;
        namesV={};
        I=0;
end
  
  %% PARSE INPUT
if ~isempty(arrival)
    namesV{i}=arrival{1};
    procDurV(i)=arrival{2};
    i=i+1;
end   
  
  %% DECIDE RPOCESS VALUE
  if ~flagPreempt
    if(isempty(procDurV))
        curProc='_';
        return;
    end
    if I==0
        [~,I]=min(procDurV);
    end
    procDurV(I)=procDurV(I)-1;
    curProc=namesV{I};
    if(procDurV(I)==0)
        procDurV(I)=[];
        namesV(I)=[];
        i=i-1;
        I=0;
    end
    return;
end


  
end
